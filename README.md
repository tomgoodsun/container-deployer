# container-deployer

Deploying docker container for web applications with name and port number specified arguments.  


## Getting started

Check out this repository and change `template/docker-compose.yml`, container settings and configurations for your environment.


## Usage

```sh
sh setup.sh [vhost_name] [web_port]
```

- Create a directory with name `[vhost_name]` if not exists in `../`, and `template/*` files are copied into it.
- Keyword `__VHOST_NAME__` in `docker-compose.yml` in copied direcotry is replaced with the first argument `[vhost_name]`.
- Keyword `__WEB_PORT__` in `docker-compose.yml` in copied direcotry is replaced with the first argument `[web_port]`.
- Automatically run `docker compose up -d` in newly created directory.
    - Older version of docker uses 'docker-compose' instead.


## Use case

This will be a one of solutions if you have to run multiple docker web container in single host.  
Using and stting up [nginx-proxy](https://hub.docker.com/r/jwilder/nginx-proxy) container in your host is strongly recommended.

Here is a sample `docker-compose.yml` for `nginx-proxy`:
```docker-compose.yml
version: '3'

services:
  nginx-proxy:
    image: jwilder/nginx-proxy:latest
    restart: always
    ports:
      - '80:80'
    volumes:
      - /var/run/docker.sock:/tmp/docker.sock:ro

networks:
  default:
    name: sample-network
```
You have to set the same value to `networks.default.name` in the both `nginx-proxy docker-compose.yml` and `template/docker-compose.yml`.
