#!/bin/sh

cd $(dirname ${0})

vhost_name=${1}
if [ "" = "${vhost_name}" ];
then
	echo 'No vhost name specified.'
	exit 1
fi

web_port=${2}
if [ "" = "${web_port}" ];
then
	echo 'No web port specified.'
	exit 1
fi

container_path=../containers/${vhost_name}
if [ ! -e ${container_path} ];
then
	mkdir -p ${container_path}
fi

cp -upvr template/* ${container_path}/
sed -i "s/__VHOST_NAME__/${vhost_name}/g" ${container_path}/docker-compose.yml
sed -i "s/__WEB_PORT__/${web_port}/g" ${container_path}/docker-compose.yml
sed -i "s/__VHOST_NAME__/${vhost_name}/g" ${container_path}/web/nginx/conf.d/default.conf

chmod -R 755 ${container_path}/
cd ${container_path}/
if command -v docker-compose &> /dev/null
then
	docker-compose build
	docker-compose up -d
else
	docker compose build
	docker compose up -d
fi

# vim:ts=4:sw=4
